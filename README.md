# Nabali Aid Network

This is the source code for the Nabali Aid Network website at https://nabali.org. 

[![Financial Contributors on Open Collective](https://opencollective.com/nabali/all/badge.svg?label=financial+contributors)](https://opencollective.com/nabali) 

## About

The Nabali Aid Network helps families flee genocide in Gaza by assisting in their fundraising efforts. This website serves as the online presence and hub for the organization.

## Tech Stack

- Next.js
- React
- TypeScript 
- Tailwind CSS with DaisyUI

## Getting Started

To install necessary packages:
```bash
pnpm i
```

To run the development server:
```bash
pnpm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the site.

## Deployment 

This Next.js app is deployed using fly.io. This repo is not directly connected to the deployed site.

## Contributing

We welcome contributions to help improve the Nabali Aid Network website. Please check the existing issues and make a merge request for the `dev` branch with your proposed changes.

For major changes, please open an issue first to discuss the updates.

## Donate

All of our donations go to our mission, not to the creation of this site. This site runs on people power.

[![Financial Contributors on Open Collective](https://opencollective.com/nabali/all/badge.svg?label=financial+contributors)](https://opencollective.com/nabali) 

<object type="image/svg+xml" data="https://opencollective.com/collective/tiers/backers.svg?avatarHeight=36&width=600"></object>