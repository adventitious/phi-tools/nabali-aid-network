"use client"
import React, { Suspense } from 'react';
import { useLanguage } from '@/hooks';
import { Header, LanguageSwitcher } from '@/components/ui';


export default function Volunteer() {
    const { language, switchLanguage } = useLanguage();

    return (
        <Suspense>
            <>
                <Header language={language} switchLanguage={switchLanguage} />
                {/* <LanguageSwitcher language={language} switchLanguage={switchLanguage} /> */}
                <div className="container px-4 py-8 mx-auto">
                    <h2 className="mb-4 text-2xl font-bold">{language === "en" ? "We Need Volunteers!" : "نحن بحاجة إلى متطوعين!"}</h2>
                    <article className="prose">
                        <p className="card bg-warning p-4"><strong>WhatsApp is required for all volunteers!</strong></p>
                        <p className="">We need volunteers to help with the following:</p>
                        <ul className="">
                            <li className="">family emotional support
                                <ul className="">
                                    <li className="">Bilingual in Arabic/English is super helpful</li>
                                </ul>
                            </li>
                            <li className="">video and graphics generation
                                <ul className="">
                                    <li className="">Experience with Canva preferred</li>
                                </ul>
                            </li>
                            <li className="">web development/automation
                                <ul className="">
                                    <li className="">Experience with Next.js/React/TailwindCSS needed to help with frontend</li>
                                    <li className="">GitLab account required</li>
                                </ul>
                            </li>
                            <li className="">data entry/management
                                <ul className="">
                                    <li className="">Bilingual in Arabic/English is super helpful</li>
                                    <li className="">Google Sheets is required</li>
                                </ul>
                            </li>
                        </ul>
                    </article>
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScpCp-Ecfxj3Cg0n8PBTHUZ1irT6dXwNetWXhPUI-TWErEtAg/viewform?embedded=true" width="640" height="978" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                </div>
            </>
        </Suspense >
    )
}