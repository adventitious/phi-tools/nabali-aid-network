"use client"
import React, { useState, useEffect, Suspense } from 'react';
import { useLanguage } from '@/hooks';
import { Header, LanguageSwitcher } from '@/components/ui';
import cardImages from "@/card-images.json"



const FilterSection = ({ language, filters, handleFilterChange }) => (
    <div className="mb-4">
        <h3 className="mb-2 text-xl font-bold">{language === "en" ? "Filter for families with..." : "الفلاتر"}</h3>
        <div className="flex flex-wrap">
            <label className="flex items-center mr-4">
                <input
                    type="checkbox"
                    className="form-checkbox"
                    checked={filters.children}
                    onChange={() => handleFilterChange('children')}
                />
                <span className="ml-2">{language === "en" ? "Children" : "الأطفال"}</span>
            </label>
            <label className="flex items-center mr-4">
                <input
                    type="checkbox"
                    className="form-checkbox"
                    checked={filters.babies}
                    onChange={() => handleFilterChange('babies')}
                />
                <span className="ml-2">{language === "en" ? "Babies" : "الرضع"}</span>
            </label>
            <label className="flex items-center mr-4">
                <input
                    type="checkbox"
                    className="form-checkbox"
                    checked={filters.seniors}
                    onChange={() => handleFilterChange('seniors')}
                />
                <span className="ml-2">{language === "en" ? "Seniors" : "كبار السن"}</span>
            </label>
            <label className="flex items-center">
                <input
                    type="checkbox"
                    className="form-checkbox"
                    checked={filters.medicalNeed}
                    onChange={() => handleFilterChange('medicalNeed')}
                />
                <span className="ml-2">{language === "en" ? "Medical Need" : "الاحتياجات الطبية"}</span>
            </label>
        </div>
    </div>
);

const ApplicantCard = ({ language, applicant, index }) => {
    console.log(cardImages.images[index % cardImages.images.length])
    return (

        <div className="shadow-xl card bg-base-100 image-full">
            {cardImages.images.length && <figure><img src={cardImages.images[index % cardImages.images.length]} /></figure>}
            <div className="card-body">
                <div className="justify-between md:justify-end card-actions">
                    {/* <a href={applicant.gfm} target="_blank" rel="noopener noreferrer" className="btn btn-outline bg-neutral">
                        {language === "en" ? "Learn more" : "اعرف المزيد"}
                    </a>
                    <a href={applicant.gfm} target="_blank" rel="noopener noreferrer" className="btn btn-secondary">
                        {language === "en" ? "Adopt" : "يعتمد"}
                    </a> */}
                    <a href={applicant.gfm} target="_blank" rel="noopener noreferrer" className="btn btn-primary">
                        {language === "en" ? "Donate" : "تبرع"}
                    </a>
                </div>
                <h3 className="p-4 rounded-lg card-title bg-neutral">{language === "en" ? applicant.names : applicant.names_ar}</h3>
                {(applicant.future_plans || applicant.future_plans_ar) && (
                    <div className="p-4 mb-4 rounded-lg bg-neutral ">
                        <h4 className="font-bold">{language === "en" ? "Help us so we can..." : "ساعدونا حتى نتمكن من..."}</h4>
                        <p>{language === "en" ? applicant.future_plans : applicant.future_plans_ar}</p>
                    </div>
                )}
                <div className="flex flex-wrap">
                    {applicant.age_of_youngest < 18 && <div className="tooltip" data-tip="Children present"><span className='mr-2 shadow-md badge badge-neutral'>🧒</span></div>}
                    {applicant.age_of_oldest > 65 && <div className="tooltip" data-tip="Seniors present"><span className='mr-2 shadow-md badge badge-neutral'>🧓</span></div>}
                    {applicant.medical_need && <div className="tooltip" data-tip="Needs medical attention"><span className='mr-2 shadow-md badge badge-neutral'>⛑️</span></div>}
                </div>
                {/* <div className="mb-4">
                <h4 className="font-bold">{language === "en" ? "About" : "نبذة"}</h4>
                <p>{language === "en" ? applicant.about : applicant.about_ar}</p>
            </div>
            {(applicant.medical_need || applicant.medical_need_ar) && (
                <div className="mb-4">
                    <h4 className="font-bold">{language === "en" ? "Medical Need" : "الاحتياجات الطبية"}</h4>
                    <p>{language === "en" ? applicant.medical_need : applicant.medical_need_ar}</p>
                </div>
            )}
            <div className="mb-4">
                <h4 className="font-bold">{language === "en" ? "Age Range" : "الفئة العمرية"}</h4>
                <p>
                    {language === "en" ? "Youngest" : "الأصغر"}: {applicant.age_of_youngest}<br />
                    {language === "en" ? "Oldest" : "الأكبر"}: {applicant.age_of_oldest}
                </p>
            </div>
            */}


            </div>
        </div>
    )
}

export default function ApplicantList() {
    const { language, switchLanguage } = useLanguage();
    const [applicants, setApplicants] = useState([]);
    const [filters, setFilters] = useState({
        children: false,
        babies: false,
        seniors: false,
        medicalNeed: false,
    });

    useEffect(() => {
        fetch('/api/families')
            .then(response => response.json())
            .then(data => setApplicants(data))
            .catch(error => console.error('Error fetching applicants:', error));
    }, []);

    const handleFilterChange = (filterName) => {
        setFilters({ ...filters, [filterName]: !filters[filterName] });
    };

    const filteredApplicants = applicants.filter(applicant =>
        (filters.children ? applicant.age_of_youngest < 18 : true) &&
        (filters.babies ? applicant.age_of_youngest < 3 : true) &&
        (filters.seniors ? applicant.age_of_oldest > 65 : true) &&
        (filters.medicalNeed ? applicant.medical_need : true)
    );



    return (
        <Suspense>
            <>
                <Header language={language} switchLanguage={switchLanguage} />
                <LanguageSwitcher language={language} switchLanguage={switchLanguage} />
                <div className="container px-4 py-8 mx-auto">
                    <h2 className="mb-4 text-2xl font-bold">{language === "en" ? "Applicant List" : "قائمة المتقدمين"}</h2>
                    <FilterSection language={language} filters={filters} handleFilterChange={handleFilterChange} />
                    <div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-3">
                        {filteredApplicants.map((applicant, index) => (
                            <ApplicantCard key={index} language={language} applicant={applicant} index={index} />
                        ))}
                    </div>
                </div>
            </>
        </Suspense >
    )
}