import React, { Suspense } from "react";
import type { Metadata } from "next";
import "./globals.css";

export const metadata: Metadata = {
  metadataBase: new URL('https://nabali.org'), 
  title: "Nabali Aid Network",
  description: "Helping families flee genocide in Gaza by assisting in their fundraising efforts.",
  keywords: ['Palestine', 'humanitarian', 'help', 'evacuate'],
  openGraph: {
    title: "Nabali Aid Network",
    description: "Join our efforts to help families in Gaza. Support the Nabali Aid Network's fundraising initiatives.",
    url: "https://nabali.org",
    type: "website",
    images: [
      {
        url: "/og-image_large.png",
        width: 1200,
        height: 630,
        alt: "Nabali Aid Network helping families",
      },
    ],
  },
  twitter: {
    cardType: "summary",
    title: "Nabali Aid Network",
    description: "Support our mission to assist families affected by the genocide in Gaza. Learn more about the Nabali Aid Network.",
    image: "/og-image_large.png",
    imageAlt: "Nabali Aid Network's efforts in Gaza",
  },
};


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className="flex flex-col justify-between w-full min-h-screen"><div>{children}</div>
        <footer className="sticky text-white bg-gray-800">
          <div className="container px-4 py-6 mx-auto">
            <p>&copy; 2024 Nabali Aid Network. All rights reserved. Built by <a className="underline" href="https://s4gu4r0.world" target="_blank" rel="noopener noreferrer">S4GU4R0</a>.</p>
          </div>
        </footer>
      </body>
    </html>


  );
}
