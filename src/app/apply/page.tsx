"use client"
import React, { useState, Suspense, useEffect } from 'react';
import { ChevronLeftIcon, ChevronRightIcon, LinkIcon, PhoneIcon, UserIcon, } from '@heroicons/react/16/solid'
import { useLanguage } from '@/hooks';
import { BilingualLabel, Header, LanguageSwitcher } from '@/components/ui';
import translatedContent from "@/translatedContent.json"


export default function Apply() {
    const { language, switchLanguage } = useLanguage();
    const [currentStep, setCurrentStep] = useState(0);

    return (
        <Suspense>
            <>
                <Header language={language} switchLanguage={switchLanguage} />
                {currentStep === 0 && <LanguageSwitcher language={language} switchLanguage={switchLanguage} />}
                <div className="flex flex-col items-center mx-2">

                    <div className="w-full p-2">
                        <EvacuationForm {...{ language, currentStep, setCurrentStep }} />
                    </div>
                </div>
            </>
        </Suspense>
    )
}


const DEFAULT_FORM_DATA = {
    familyMembers: [''],
    about: '',
    about_ar: '',
    gfm: '',
    ageOfYoungest: 0,
    ageOfOldest: 0,
    medicalNeed: '',
    medicalNeed_ar: '',
    futurePlans: '',
    futurePlans_ar: '',
    tiktok: '',
    okToShareTikTok: false,
    instagram: '',
    okToShareInstagram: false,
    advocate: '',
    advocate_ar: '',
    whatsapp: '',
    understandsEligibility: false,
}
const DEFAULT_FORM_ERRORS_DATA = {
    familyMembers: "",
    about: "",
    gfm: "",
    ageOfYoungest: "",
    ageOfOldest: "",
    medicalNeed: "",
    futurePlans: "",
    tiktok: "",
    okToShareTikTok: "",
    instagram: "",
    okToShareInstagram: "",
    advocate: "",
    whatsapp: "",
    understandsEligibility: "",
}

const EvacuationForm = ({ language, currentStep, setCurrentStep }: { language: string, currentStep: number, setCurrentStep: any }) => {
    const [loadingData, setLoadingData] = useState({
        isLoading: false,
        error: false,
        isSubmitted: false
    })
    const [formData, setFormData] = useState(DEFAULT_FORM_DATA);
    const [errors, setErrors] = useState(DEFAULT_FORM_ERRORS_DATA);
    const [hasError, setHasError] = useState(false); // for each section of the form before moving on to the next section
    const [touched, setTouched] = useState({});

    const [nextButtonIsDisabled, setNextButtonIsDisabled] = useState(false);

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleFamilyMemberChange = (index, value) => {
        const updatedFamilyMembers = [...formData.familyMembers];
        updatedFamilyMembers[index] = value;
        setFormData({ ...formData, familyMembers: updatedFamilyMembers });
    };

    const addFamilyMember = () => {
        setFormData({ ...formData, familyMembers: [...formData.familyMembers, ''] });
    };

    const removeFamilyMember = (index) => {
        const updatedFamilyMembers = [...formData.familyMembers];
        updatedFamilyMembers.splice(index, 1);
        setFormData({ ...formData, familyMembers: updatedFamilyMembers });
    };

    const validateSection = () => {
        let sectionErrors = {};
        if (currentStep === 0) {
            // Basic Information section validation
            if (formData.advocate.trim() === '') {
                sectionErrors.advocate = true;
            }
            if (formData.whatsapp.trim() === '') {
                console.log('validating phone number presence')
                sectionErrors.whatsapp = 'Phone number is required';
            } else if (!/^\d+$/.test(formData.whatsapp.trim()) || formData.whatsapp.trim().length < 4 || formData.whatsapp.trim().length > 12) {
                sectionErrors.whatsapp = 'Invalid phone number';
            }
            if (formData.gfm.trim() === '') {
                sectionErrors.gfm = true;
            }
            if (formData.understandsEligibility === false) {
                sectionErrors.understandsEligibility = true;
            }
        } else if (currentStep === 1) {
            // Family Details section validation
            if (formData.familyMembers.some(member => member.trim() === '')) {
                sectionErrors.familyMembers = true;
            }
            if (formData.about.trim() === '') {
                sectionErrors.about = true;
            }
            if (formData.ageOfYoungest < 0) {
                sectionErrors.ageOfYoungest = true;
            }
            if (formData.ageOfOldest < 0) {
                sectionErrors.ageOfOldest = true;
            }
        }

        setErrors(sectionErrors);
        // @ts-ignore
        const errorsPresent = Object.keys(sectionErrors).some(key => sectionErrors[key])
        setHasError(errorsPresent)
        // @ts-ignore
        return errorsPresent
    };




    const handleSubmit = (e: any) => {
        e.preventDefault();
        let data = formData
        data.language = language
        if (!hasError) {
            console.log(formData);
            setLoadingData({ isLoading: true, error: false })

            fetch('/api/families', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formData),
            }).then(() => {
                console.log('worked')
                setLoadingData({ isLoading: false, error: false, isSubmitted: true })
                setFormData(DEFAULT_FORM_DATA)
            }).catch(() => {
                console.log('worked')
                setLoadingData({ isLoading: false, error: true, isSubmitted: false })
                setFormData(data)
            })
        }
    };

    const handleBlur = (field: string) => {
        validateSection()
        setTouched({ ...touched, [field]: true });
    };


    const steps = translatedContent.main.form.steps.map(step => ({
        title: step.title[language],
        content: (
            <>
                {currentStep === 0 && (
                    <BasicInformationFields
                        language={language}
                        formData={formData}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        setFormData={setFormData}
                    />
                )}
                {currentStep === 1 && (
                    <FamilyDetailsFields
                        language={language}
                        formData={formData}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        handleFamilyMemberChange={handleFamilyMemberChange}
                        addFamilyMember={addFamilyMember}
                        removeFamilyMember={removeFamilyMember}
                    />
                )}
                {currentStep === 2 && (
                    <SocialMediaFields
                        language={language}
                        formData={formData}
                        errors={errors}
                        touched={touched}
                        handleChange={handleChange}
                        handleBlur={handleBlur}
                        setFormData={setFormData}
                    />
                )}
            </>
        )
    }));


    const nextStep = () => {
        const isInvalid = validateSection();
        setHasError(isInvalid);

        if (isInvalid) {
            console.log('has errors', isInvalid)
        } else {
            if (currentStep < steps.length - 1) {
                setCurrentStep(currentStep + 1);
            }
        }
    };

    const prevStep = () => {
        console.log(currentStep)
        if (currentStep > 0) {
            setCurrentStep(currentStep - 1);
        }
    };

    const nextIsDisabled = () => {
        if (currentStep === 0) {
            // Basic Information section validation
            if (formData.advocate.trim() === '' || formData.whatsapp.trim() === '' || formData.gfm.trim() === '' || formData.understandsEligibility === false) {
                setNextButtonIsDisabled(true)
                return true
            } else {
                setNextButtonIsDisabled(false)
                return false
            }
        } else if (currentStep === 1) {
            // Family Details section validation
            if (formData.familyMembers.some(member => member.trim() === '') || formData.about.trim() === '' || formData.ageOfYoungest < 0 || formData.ageOfOldest < 0) {
                setNextButtonIsDisabled(true)
                return true
            } else {
                setNextButtonIsDisabled(false)
                return false
            }
        }
        return false
    }

    useEffect(() => {
        nextIsDisabled()
    }, [formData])

    return (
        <div className="container px-4 py-8 mx-auto" dir={language === "en" ? "ltr" : "rtl"}>
            <LoadingMessage loadingData={loadingData} language={language} />

            {!loadingData.isSubmitted && (<>
                <DataPrivacyMessage language={language} />
                <form onSubmit={handleSubmit} className={`${language === 'ar' ? 'text-right' : ''}`}>
                    <StepProgress steps={steps} currentStep={currentStep} />
                    <div className="relative mt-8">
                        {steps[currentStep].content}
                    </div>
                    <StepNavigation
                        currentStep={currentStep}
                        steps={steps}
                        prevStep={prevStep}
                        nextStep={nextStep}
                        hasError={hasError}
                        nextButtonIsDisabled={nextButtonIsDisabled}
                        language={language}
                        handleSubmit={handleSubmit}
                        isLoading={loadingData.isLoading}
                    />
                </form>
            </>)}
        </div>
    );
};

function BasicInformationFields({ language, formData, errors, touched, handleChange, handleBlur, setFormData }) {
    const { fields } = translatedContent.main.form;

    return (
        <>
            {/* Basic Information fields */}
            <div className="">
                <div className="mb-4">
                    <label htmlFor="advocate" className={`flex items-center mb-2 font-bold ${errors.advocate ? 'text-error' : ''}`}>
                        <UserIcon className="w-6 h-6" /> {fields.advocate.label[language]}*
                    </label>
                    <input
                        type="text"
                        id="advocate"
                        name={language === "en" ? "advocate" : "advocate_ar"}
                        className={`input input-bordered w-full ${errors?.advocate && touched?.advocate ? 'input-error' : ''}`}
                        value={formData.advocate}
                        onChange={handleChange}
                        onBlur={() => handleBlur('advocate')}
                        required
                    />
                </div>
                <div className="mb-4">
                    <BilingualLabel required={true} htmlFor="whatsapp" emoji={<PhoneIcon className="w-6 h-6" />} englishContent={fields.whatsapp.label.en} arabicContent={fields.whatsapp.label.ar} language={language} error={errors?.whatsapp && touched?.whatsapp} />
                    <input
                        type="tel"
                        id="whatsapp"
                        name="whatsapp"
                        className={`input input-bordered w-full ${(!!errors?.whatsapp && touched?.whatsapp) ? 'input-error' : ''}`}
                        value={formData.whatsapp}
                        onChange={handleChange}
                        onBlur={() => handleBlur('whatsapp')}
                        placeholder={fields.whatsapp.placeholder}
                        required
                    />
                    {errors?.whatsapp && touched?.whatsapp && (
                        <span className="mt-1 text-error">
                            {language === 'en' ? fields.whatsapp.error.en : fields.whatsapp.error.ar}
                        </span>
                    )}
                </div>
                <div className="mb-4">
                    <BilingualLabel required={true} htmlFor="gfm" emoji={<LinkIcon className="w-6 h-6" />} englishContent={fields.gfm.label.en} arabicContent={fields.gfm.label.ar} language={language} error={errors?.gfm && touched?.gfm} />
                    <input
                        type="text"
                        id="gfm"
                        name="gfm"
                        className={`input input-bordered w-full ${errors?.gfm && touched?.gfm ? 'input-error' : ''}`}
                        value={formData.gfm}
                        onChange={handleChange}
                        onBlur={() => handleBlur('gfm')}
                        required
                    />
                    {errors?.gfm && touched?.gfm && (
                        <span className="mt-1 text-error">
                            {language === 'en' ? fields.gfm.error.en : fields.gfm.error.ar}
                        </span>
                    )}
                </div>
                <div className="mb-4">
                    <div className="form-control">
                        <label className={`label cursor-pointer ${errors?.understandsEligibility && touched?.understandsEligibility ? 'text-error' : ''}`}>
                            <input
                                type="checkbox"
                                className={`checkbox ${errors?.understandsEligibility && touched?.understandsEligibility ? 'checkbox-error' : ''}`}
                                checked={formData.understandsEligibility}
                                onChange={(e) => setFormData({ ...formData, understandsEligibility: e.target.checked })}
                                onBlur={() => handleBlur('understandsEligibility')}
                                required
                            />
                            <span className={`ml-2 font-bold label-text ${errors?.understandsEligibility && touched?.understandsEligibility ? 'text-error' : ''}`}>
                                {fields.understandsEligibility.label[language]}*
                            </span>
                        </label>
                    </div>
                </div>
            </div>
            {/* ... */}
        </>
    );
}
function FamilyDetailsFields({ language, formData, errors, touched, handleChange, handleBlur, handleFamilyMemberChange, addFamilyMember, removeFamilyMember }) {
    const { fields } = translatedContent.main.form;
    return (<>
        {/* Family Details fields */}
        <div className="">
            <div className="mb-4">
                <label className={`label cursor-pointer font-bold ${errors?.familyMembers && touched?.familyMembers ? 'text-error' : ''}`}>
                    🔠 {fields.familyMembers.label[language]}*
                </label>

                {formData.familyMembers.map((member, index) => (
                    <div key={index} className="flex items-center mb-2">
                        <input
                            type="text"
                            className={`input input-bordered w-full ${errors?.familyMembers && touched?.familyMembers ? 'text-error' : ''}`}
                            value={member}
                            onChange={(e) => handleFamilyMemberChange(index, e.target.value)}
                            onBlur={() => handleBlur('familyMembers')}
                            required
                        />
                        {index > 0 && (
                            <button
                                type="button"
                                className="ml-2 btn btn-error btn-sm"
                                onClick={() => removeFamilyMember(index)}
                            >
                                {fields.familyMembers.remove[language]}
                            </button>
                        )}
                    </div>
                ))}
                <button type="button" className="btn btn-info btn-sm" onClick={addFamilyMember}>
                    {fields.familyMembers.addPerson[language]}
                </button>
                {(errors?.familyMembers && touched?.familyMembers) && <span className="text-error">{fields.familyMembers.error[language]}</span>}
            </div>
            <div className="mb-4">
                <label className={`block mb-2 font-bold ${errors?.about && touched?.about ? 'text-error' : ''}`}>
                    📝 {fields.about.label[language]}*
                </label>
                <textarea
                    id="about"
                    name={language === "en" ? "about" : "about_ar"}
                    className={`textarea textarea-bordered w-full ${errors?.about && touched?.about ? 'textarea-error' : ''}`}
                    value={formData.about}
                    onChange={handleChange}
                    onBlur={() => handleBlur('about')}
                    required
                ></textarea>
                {(errors?.about && touched?.about) && (
                    <span className="mt-1 text-error">
                        {fields.about.error[language]}
                    </span>
                )}
            </div>
            <div className="mb-4">
                <BilingualLabel htmlFor="ageOfYoungest" emoji="🧓" englishContent={fields.ageOfYoungest.label.en} arabicContent={fields.ageOfYoungest.label.ar} language={language} required={true} error={errors?.ageOfYoungest && touched?.ageOfYoungest} />
                <input
                    type="number"
                    id="ageOfYoungest"
                    name="ageOfYoungest"
                    className={`input input-bordered w-full ${(errors?.ageOfYoungest && touched?.ageOfYoungest) || formData.ageOfYoungest < 0 ? 'input-error' : ''}`}
                    value={formData.ageOfYoungest}
                    onChange={handleChange}
                    onBlur={() => handleBlur('ageOfYoungest')}
                    min={0}
                    required
                />
                {((errors?.ageOfYoungest && touched?.ageOfYoungest) || formData.ageOfYoungest < 0) && (
                    <span className="mt-1 text-error">
                        {fields.ageOfYoungest.error[language]}
                    </span>
                )}
            </div>
            <div className="mb-4">
                <BilingualLabel htmlFor="ageOfOldest" emoji="🧓" englishContent={fields.ageOfOldest.label.en} arabicContent={fields.ageOfOldest.label.ar} language={language} required={true} error={errors?.ageOfOldest && touched?.ageOfOldest} />
                <input
                    type="number"
                    id="ageOfOldest"
                    name="ageOfOldest"
                    className={`input input-bordered w-full ${(errors?.ageOfOldest && touched?.ageOfOldest) || formData.ageOfOldest < 0 ? 'input-error' : ''}`}
                    value={formData.ageOfOldest}
                    onChange={handleChange}
                    onBlur={() => handleBlur('ageOfOldest')}
                    min={0}
                    required
                />
                {((errors?.ageOfOldest && touched?.ageOfOldest) || formData.ageOfOldest < 0) && (
                    <span className="mt-1 text-error">
                        {fields.ageOfOldest.error[language]}
                    </span>
                )}
            </div>
            <div className="mb-4">
                <BilingualLabel htmlFor="medicalNeed" emoji="⚕️" englishContent={fields.medicalNeed.label.en} arabicContent={fields.medicalNeed.label.ar} language={language} required={false} error={errors?.medicalNeed && touched?.medicalNeed} />
                <textarea
                    id="medicalNeed"
                    name={language === "en" ? "medicalNeed" : "medicalNeed_ar"}
                    className="w-full textarea textarea-bordered"
                    value={formData.medicalNeed}
                    onChange={handleChange}
                    onBlur={() => handleBlur('medicalNeed')}
                    placeholder={fields.medicalNeed.placeholder[language]}
                ></textarea>
            </div>
            <div className="mb-4">
                <BilingualLabel error={errors?.futurePlans && touched?.futurePlans} required={false} htmlFor="futurePlans" emoji="❤️" englishContent={fields.futurePlans.label.en} arabicContent={fields.futurePlans.label.ar} language={language} />
                <textarea
                    id="futurePlans"
                    name={language === "en" ? "futurePlans" : "futurePlans_ar"}
                    className="w-full textarea textarea-bordered"
                    value={formData.futurePlans}
                    onChange={handleChange}
                    onBlur={() => handleBlur('futurePlans')}
                    placeholder={fields.futurePlans.placeholder[language]}
                ></textarea>
            </div>
        </div>
        {/* ... */}
    </>)
}

function SocialMediaFields({ language, formData, errors, touched, handleChange, handleBlur, setFormData }) {
    const { fields } = translatedContent.main.form;
    return (
        <>
            {/* Social Media fields */}
            <div className="">
                <div className="mb-4">
                    <BilingualLabel error={errors?.tiktok && touched?.tiktok} required={false} htmlFor="tiktok" emoji="🤳" englishContent='TikTok' arabicContent='تيك توك' language={language} />
                    <input
                        type="text"
                        id="tiktok"
                        name="tiktok"
                        className="w-full input input-bordered"
                        value={formData.tiktok}
                        onChange={handleChange}
                        onBlur={() => handleBlur('tiktok')}
                        placeholder={language === "en" ? "username" : "اسم المستخدم"}
                    />
                    <div className="form-control">
                        <label className="cursor-pointer label">
                            <span className="label-text">
                                {language === 'en' ? 'Display TikTok on the site?' : 'عرض التيك توك على الموقع؟'}
                            </span>
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={formData.okToShareTikTok}
                                onChange={(e) => setFormData({ ...formData, okToShareTikTok: e.target.checked })}
                            />
                        </label>
                    </div>
                </div>
                <div className="mb-4">
                    <BilingualLabel htmlFor="instagram" emoji="🤳" englishContent='Instagram' arabicContent='انستغرام' language={language} required={false} error={errors?.instagram && touched?.instagram} />
                    <input
                        type="text"
                        id="instagram"
                        name="instagram"
                        className="w-full input input-bordered"
                        value={formData.instagram}
                        onChange={handleChange}
                        onBlur={() => handleBlur('instagram')}
                        placeholder={language === "en" ? "username" : "اسم المستخدم"}
                    />
                    <div className="form-control">
                        <label className="cursor-pointer label">
                            <span className="label-text">
                                {language === 'en' ? 'Display Instagram on the site?' : 'عرض الانستغرام على الموقع؟'}
                            </span>
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={formData.okToShareInstagram}
                                onChange={(e) => setFormData({ ...formData, okToShareInstagram: e.target.checked })}
                            />
                        </label>
                    </div>
                </div>
            </div>
            {/* ... */}
        </>
    );
}

function LoadingMessage({ loadingData, language }) {
    return (
        <>
            {loadingData.isSubmitted && <div className='p-4 m-4 font-bold card bg-success'>{language === "en" ? "We got your application and your fundraiser will be on the site soon! We will reach out to you on WhatsApp to stay in touch." : "لقد حصلنا على طلبك وستكون حملة جمع التبرعات الخاصة بك على الموقع قريبًا! سوف نتواصل معك على WhatsApp للبقاء على اتصال."}</div>}
            {loadingData.error && <div className='p-4 m-4 font-bold card bg-error'>{language === "en" ? "There was a problem when submitted. Please double check the information in the form and try again." : "كانت هناك مشكلة عند تقديمها. يرجى التحقق مرة أخرى من المعلومات الموجودة في النموذج والمحاولة مرة أخرى."}</div>}
        </>
    );
}

function DataPrivacyMessage({ language }) {
    return (
        <p className="mb-4">
            {language === 'en' ? 'We do NOT sell your data, and we never will. We only share information about you when telling your story to raise money to help you.' : 'نحن لا نبيع بياناتك، ولن نفعل ذلك أبدًا. نحن نشارك المعلومات عنك فقط عند سرد قصتك لجمع الأموال لمساعدتك.'}
        </p>
    );
}

function StepProgress({ steps, currentStep }) {
    return (
        <ul className="w-full steps steps-responsive">
            {steps.map((step, index) => (
                <li key={index} className={`step ${index <= currentStep ? 'step-primary' : ''}`}>
                    <span className="hidden sm:block">{step.title}</span>
                    <span className="sm:hidden">{index + 1}</span>
                </li>
            ))}
        </ul>
    );
}

function StepNavigation({ currentStep, steps, prevStep, nextStep, hasError, nextButtonIsDisabled, language, handleSubmit, isLoading }) {
    const [isConfirmed, setConfirmation] = useState(false);

    const handleConfirmation = () => {
        setConfirmation(!isConfirmed);
    };
    if (isLoading) {
        return (
            <span className="loading loading-spinner text-primary"></span>
        )
    } else {

        return (
            <div className="mt-16 ">
                {currentStep === steps.length - 1 && (<div className="form-control">
                    <label className="font-bold cursor-pointer label">
                        <span className="label-text">{language === 'en' ? 'I confirm I am ready to submit' : 'أؤكد أنني على استعداد للتقديم'}</span>
                        <input
                            type="checkbox"
                            className="checkbox"
                            checked={isConfirmed}
                            onChange={handleConfirmation}
                        />
                    </label>
                </div>)}
                <div className='flex justify-between mt-4'>
                    {currentStep > 0 ? (
                        <button type="button" className="btn btn-secondary" onClick={prevStep}>
                            <ChevronLeftIcon className="w-6 h-6" /> {language === 'en' ? 'Previous' : 'السابق'}
                        </button>
                    ) : (
                        <div className="w-1/2" />
                    )}
                    {currentStep < steps.length - 1 ? (
                        <button
                            type="button"
                            className={`btn flex items-center justify-around min-w-32 btn-primary ${hasError ? 'animate-shake' : ''} ${nextButtonIsDisabled ? 'btn-disabled' : ''}`}
                            onClick={nextStep}
                            disabled={nextButtonIsDisabled}
                        >
                            <span>{language === 'en' ? 'Next' : 'التالي'}</span> <ChevronRightIcon className="w-6 h-6" />
                        </button>
                    ) : (
                        <div className="flex items-center">
                            <button
                                type="submit"
                                className={`font-bold btn btn-primary lg:w-1/4 ${!isConfirmed ? 'btn-disabled' : ''}`}
                                disabled={!isConfirmed}
                                onClick={handleSubmit}
                            >
                                {language === 'en' ? 'Confirm Submission' : 'تأكيد التقديم'}
                            </button>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}