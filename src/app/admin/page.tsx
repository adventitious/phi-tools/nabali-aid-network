"use client"
import React, { useEffect, useState } from "react"
const { parsePhoneNumber } = require('libphonenumber-js');
import { Video } from "@/components/ui"


export default function Admin() {
    const defaultEdits = {
        // OOBCheckDone: "FALSE",
        hasWhatsAppGroup: "FALSE",
        invitedFamilyToWhatsAppGroup: "FALSE",
        publish: "FALSE"
    }
    const [applicants, setApplicants] = useState([]);
    const [updates, setUpdates] = useState(defaultEdits);
    const [currentFamilyIndex, setCurrentFamilyIndex] = useState(0);
    const [currentStep, setCurrentStep] = useState(0);

    useEffect(() => {
        //@ts-ignore
        let results = []
        fetch('/api/admin')
            .then(response => response.json())
            .then(data => {
                results = data
                setApplicants(results)
            })
            .catch(error => console.error('Error fetching applicants:', error));
    }, []);

    const currentFamily = applicants[currentFamilyIndex];


    const handleSubmit = () => {
        // Handle form submission logic
        setCurrentFamilyIndex((prevIndex) => prevIndex + 1);
        setCurrentStep(0)
        console.log({
            id: applicants[currentFamilyIndex].id,
            updates
        })
        fetch("/api/admin", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                id: applicants[currentFamilyIndex].id,
                updates
            }),
        })
            .then(res => res.json())
            .then(res => console.log({ res }))
            .catch(err => console.error(err))

    };

    if (!currentFamily || currentFamilyIndex > applicants.length - 1) {
        return <div>No more families to process.</div>;
    }

    return (
        <div className="container p-4 mx-auto">
            <h1 className="mb-4 text-2xl font-bold">Admin Portal</h1>
            <div className="flex justify-end mb-10"><button onClick={() => setCurrentFamilyIndex((prevIndex) => prevIndex + 1)} className="btn btn-secondary">Skip</button></div>
            <div className="shadow-xl card bg-neutral-300">
                <div className="card-body">
                    <h2 className="ml-4 text-5xl card-title">Current Family</h2>
                    <div className="flex items-center">
                        <div className="flex items-center justify-center m-2 text-4xl font-bold bg-primary btn-circle min-w-16 min-h-16">{currentFamily.id}</div>
                        <h2 className="ml-4 card-title">{currentFamily.names}</h2>
                    </div>

                    {currentStep === 0 && <div className="p-4 mt-4 card bg-neutral">
                        <h3 className="mb-2 text-3xl font-bold">Step 1: Create WhatsApp Group</h3>
                        <h4 className="mb-4 text-xl">Create a WhatsApp group using <strong>their last name</strong> and <strong>record number</strong>:</h4>
                        <div style={{ minHeight: "300px" }} className="flex justify-center">
                            <Video src="/createGroup.mp4" />
                        </div>
                        <h5 className="text-lg font-bold">Example:</h5>
                        <div className="p-4 my-2 border-info card-bordered">LastName {currentFamily.id}</div>
                        <h5 className="text-lg font-bold">Applicant data:</h5>
                        <div className="p-4 mt-2 font-bold border card-compact bg-info">{`${currentFamily?.names?.split(",")[0]} ${currentFamily.id}`}</div>
                        <button className="mt-2 font-bold shadow-lg btn btn-warning" onClick={() => {
                            setUpdates({ ...updates, hasWhatsAppGroup: "TRUE" })
                            setCurrentStep(1)
                        }}>
                            Mark Done ✅
                        </button>
                    </div>}

                    {currentStep === 1 && <div className="p-4 mt-4 card bg-neutral">
                        <h3 className="mb-2 text-3xl font-bold">Step 2: Invite People to WhatsApp Group</h3>
                        <h4 className="mb-4 text-xl">Use the phone number below to find the contact person and add them the WhatsApp group.</h4>
                        <div className="p-4 text-sm font-bold border-warning card-bordered">⚠️ WhatsApp will say you're sending an invite to the group instead of directly adding them. This the only way to keep YOUR phone number private.</div>
                        <div style={{ minHeight: "300px" }} className="flex justify-center">
                            <Video src="/addMembers.mp4" />
                        </div>
                        <h5 className="text-lg font-bold">Applicant data:</h5>
                        <div className="p-4 mt-2 font-bold border card-compact bg-info">{currentFamily.formatted_whatsapp}</div>
                        <button className="mt-2 font-bold shadow-lg btn btn-warning" onClick={() => {
                            setUpdates({ ...updates, invitedFamilyToWhatsAppGroup: "TRUE", publish: "TRUE" })
                            setCurrentStep(2)
                        }}>
                            Mark Done ✅
                        </button>
                    </div>
                    }
                    {currentStep === 2 && <div className="p-4 mt-4 card card-bordered">
                        <h3 className="mb-2 text-3xl font-bold">Step 3: Confirm Your Work</h3>
                        <p className="my-4 text-2xl text-center">You did it! 🥳</p>
                        <p className="mb-2">Pressing the button below will record in the database that these steps were done.</p>
                        <button className="shadow-lg btn btn-primary" onClick={handleSubmit}>
                            Publish to site
                        </button>
                    </div>}
                </div>
            </div>
        </div>
    )
}

