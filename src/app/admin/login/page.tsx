'use client';
import { useState } from 'react';
import { useRouter } from 'next/navigation';

export default function AdminLogin() {
  const [password, setPassword] = useState('');
  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    const response = await fetch('/api/admin/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ password }),
    });

    if (response.ok) {
      router.push('/admin');
    } else {
      alert('Invalid password');
    }
  };

  return (
    <div className="w-screen h-5/6">
      <h1>Admin Login</h1>
      <form onSubmit={handleSubmit} className="flex flex-col items-center justify-center mx-auto my-48">
        <input
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button type="submit" className="my-4 btn btn-primary">Login</button>
      </form>
    </div>
  );
}