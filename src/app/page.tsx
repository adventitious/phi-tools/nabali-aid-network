"use client"
import React, { Suspense } from "react"
import { useLanguage } from "@/hooks";
import { LanguageSwitcher } from "@/components/ui";
import translatedContent from "@/translatedContent.json";

export default function Home() {
  const { language, switchLanguage } = useLanguage();
  return (
    <Suspense>
      <>
        <header className="bg-white shadow">
          <div className="container flex items-center px-4 py-6 mx-auto">
            <a href="/">
              <h1 className="text-3xl font-bold text-gray-800">{translatedContent.header.title[language]}</h1>
            </a>
          </div>
        </header>
        <LanguageSwitcher language={language} switchLanguage={switchLanguage} />
        <div className="flex flex-col items-center mx-2">
          <div className="w-full p-2 ">
            <LandingPage {...{ language }} />
          </div>
        </div>
      </>
    </Suspense>
  )
}

const LandingPage = ({ language }: { language: string }) => {
  return (
    <div className="min-h-screen bg-gray-100">
      <main className="container px-4 py-8 mx-auto">
        <section className="mb-12">
          <div className="w-48 h-48 mx-auto">
            <svg id="Best_one" data-name="Best one" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 834 772">
              <defs>
                <clipPath id="clippath">
                  <circle className="cls-3" cx="418.85" cy="399.61" r="198.62" />
                </clipPath>
              </defs>
              <g className="cls-9">
                <g>
                  <path className="cls-4" d="M1055,473.92c0,47.25-.06,94.5.11,141.75.01,4.14-.94,5.07-5.07,5.07-292.08-.11-584.17-.1-876.25-.1.64-2.17,2.76-2.82,4.3-3.98,37.67-28.32,75.36-56.61,113.09-84.85,25.98-19.45,52-38.83,78.01-58.24,2.57.03,5.14.09,7.72.09,223.65,0,447.31,0,670.96.01,2.38,0,4.76.16,7.14.25Z" />
                  <path className="cls-1" d="M174.89,179.39c291.72,0,583.43.01,875.15-.1,4.13,0,5.09.93,5.07,5.07-.17,47.25-.11,94.5-.11,141.75-2.2.09-4.39.26-6.59.26-224.02.01-448.04.01-672.06.01-2.39,0-4.78.06-7.16.09-.63-.38-1.29-.71-1.88-1.14-63.5-47.61-127.01-95.23-190.47-142.9-.92-.69-2.89-1.02-1.95-3.03Z" />
                  <path className="cls-5" d="M369.19,326.46c2.39-.03,4.78-.09,7.16-.09,224.02,0,448.04,0,672.06-.01,2.2,0,4.39-.17,6.59-.26v147.82c-2.38-.09-4.76-.25-7.14-.25-223.65-.01-447.31-.01-670.96-.01-2.57,0-5.14-.06-7.72-.09,1.71-1.38,3.38-2.82,5.13-4.14,29.52-22.15,59.01-44.33,88.61-66.36,3.34-2.49,3.34-3.71-.02-6.11-9.1-6.52-17.99-13.34-26.94-20.08-22.27-16.79-44.52-33.6-66.78-50.41Z" />
                  <path className="cls-2" d="M369.19,326.46c22.26,16.8,44.51,33.62,66.78,50.41,8.94,6.74,17.83,13.56,26.94,20.08,3.36,2.4,3.36,3.63.02,6.11-29.61,22.03-59.1,44.21-88.61,66.36-1.76,1.32-3.42,2.76-5.13,4.14-26.01,19.41-52.03,38.79-78.01,58.24-37.72,28.25-75.42,56.53-113.09,84.85-1.54,1.16-3.65,1.81-4.3,3.98-2.28-.99-1.03-2.98-1.04-4.41-.08-63.35-.07-126.7-.07-190.05,0-80.79-.01-161.59.11-242.38,0-1.39-2.26-4.75,2.09-4.4-.93,2.01,1.04,2.35,1.95,3.03,63.47,47.66,126.97,95.28,190.47,142.9.58.44,1.25.77,1.88,1.14Z" />
                </g>
              </g>
              <path className="cls-7" d="M418.85,209.41c50.8,0,98.57,19.78,134.49,55.71,35.92,35.92,55.71,83.69,55.71,134.49s-19.78,98.57-55.71,134.49c-35.92,35.92-83.69,55.71-134.49,55.71s-98.57-19.78-134.49-55.71-55.71-83.69-55.71-134.49,19.78-98.57,55.71-134.49,83.69-55.71,134.49-55.71M418.85,145.21c-140.5,0-254.4,113.9-254.4,254.4s113.9,254.4,254.4,254.4,254.4-113.9,254.4-254.4-113.9-254.4-254.4-254.4h0Z" />
              <g>
                <path className="cls-8" d="M320.05,581.41l-19.52,30.69-6.13-3.9,1.18-28.37c-.32.53-.52.89-.61,1.06-.27.56-.53,1.02-.75,1.38l-11.68,18.37-5.6-3.56,19.52-30.69,7.75,4.93-1.07,24.74c.48-.96.87-1.67,1.17-2.13l10.21-16.05,5.55,3.53Z" />
                <path className="cls-8" d="M350.79,602.34l-16.02-6.59-2.84,6.9,12.59,5.18-2.61,6.33-12.59-5.18-3.02,7.34,16.02,6.59-2.74,6.65-24.03-9.88,13.83-33.64,24.03,9.88-2.63,6.41Z" />
                <path className="cls-8" d="M384.04,603.52l-1.66,7-8.76-2.08-6.73,28.39-8.4-1.99,6.73-28.39-8.82-2.09,1.66-7,25.98,6.16Z" />
                <path className="cls-8" d="M435.63,606.64l-10.28,36.06-8.37-.26-5.22-24.24-6.53,23.87-8.3-.26-8.2-36.65,8.69.28,4.31,22.34c.01.11.1.5.26,1.18.06.25.15.63.26,1.13l6.73-24.29,7.86.25,5.34,24.67c.14-.46.34-1.21.6-2.27l5.86-22.02,7,.22Z" />
                <path className="cls-8" d="M461.55,639.2c-4.82.86-8.88-.29-12.16-3.44-3.28-3.15-5.38-7.3-6.29-12.44-.93-5.23-.37-9.83,1.69-13.8,2.05-3.97,5.55-6.4,10.5-7.28,5-.89,9.1.31,12.31,3.59,3.21,3.28,5.24,7.33,6.09,12.14.92,5.19.39,9.81-1.6,13.84-1.99,4.03-5.5,6.5-10.53,7.39ZM456.45,609.09c-4.4.78-5.91,5.04-4.54,12.77,1.38,7.75,4.26,11.23,8.66,10.45,4.26-.76,5.7-5,4.33-12.73-1.38-7.75-4.19-11.24-8.45-10.49Z" />
                <path className="cls-8" d="M494.56,615.18l5.12,13.48-8.02,3.05-12.91-34,14.95-5.68c2.76-1.05,5.09-1.57,7-1.56,1.91,0,3.74.65,5.48,1.95,1.75,1.29,3.02,2.99,3.81,5.08,1.55,4.08.7,7.76-2.57,11.03l11.99,12.65-8.8,3.34-10.34-11.49-5.71,2.17ZM489.07,600.71l3.29,8.67,4.88-1.85c1.59-.61,2.71-1.16,3.35-1.66.64-.5,1.09-1.24,1.36-2.23.27-.99.22-1.98-.16-2.97-.36-.95-.96-1.69-1.81-2.22-.85-.53-1.64-.76-2.37-.68-.73.08-2.01.46-3.84,1.16l-4.71,1.79Z" />
                <path className="cls-8" d="M521.83,578.59l8.91,14.58.82-20.53,6.96-4.25-.69,15.64,21.51,14.26-8.01,4.89-14.43-9.7-.47,9.02,4.35,7.12-7.14,4.36-18.96-31.04,7.14-4.36Z" />
              </g>
              <g>
                <path className="cls-6" d="M280.4,203.77l22.71,28.41-5.68,4.54-26.46-10.3c.4.48.67.78.81.92.44.44.8.83,1.06,1.16l13.59,17.01-5.18,4.14-22.71-28.41,7.17-5.73,23.06,9.03c-.75-.77-1.29-1.36-1.64-1.79l-11.88-14.86,5.14-4.11Z" />
                <path className="cls-6" d="M311.24,185.64l27.09,26.81-8.09,4.29-5.49-5.57-10.39,5.51,1.49,7.68-6.6,3.5-6.73-37.6,8.72-4.62ZM313.02,209.78l6.81-3.61-9.35-9.49,2.54,13.1Z" />
                <path className="cls-6" d="M334.87,174.9l16.2-4.98c2.9-.89,5.19-1.21,6.87-.94,1.68.26,3.2.91,4.56,1.94,1.36,1.03,2.33,2.5,2.91,4.4,1.18,3.85-.11,7.06-3.89,9.64,2.58-.2,4.72.32,6.42,1.57,1.7,1.24,2.85,2.82,3.43,4.72.8,2.6.51,5.15-.88,7.65-1.39,2.5-4.3,4.43-8.73,5.8l-16.2,4.98-10.7-34.76ZM344.88,178.54l2.34,7.62,6.16-1.9c2.26-.69,3.54-1.58,3.85-2.65.31-1.07.34-2.01.09-2.83-.27-.88-.75-1.57-1.43-2.06-.68-.49-1.33-.72-1.96-.71-.63.02-1.79.29-3.49.81l-5.58,1.72ZM349.13,192.34l2.58,8.38,6.25-1.92c2.13-.66,3.53-1.49,4.18-2.51.65-1.02.77-2.2.36-3.54-.35-1.12-1.07-1.96-2.16-2.51-1.1-.55-2.63-.53-4.6.08l-6.61,2.03Z" />
                <path className="cls-6" d="M397.25,161.11l15.3,34.9-9.1,1-3.05-7.2-11.69,1.29-1.45,7.69-7.42.82,7.6-37.43,9.81-1.08ZM390.01,184.21l7.66-.85-5.2-12.27-2.46,13.11Z" />
                <path className="cls-6" d="M429.53,189.64l14.45.82-.41,7.18-23.07-1.31,2.07-36.31,8.62.49-1.66,29.13Z" />
                <path className="cls-6" d="M467.16,165.04l-6.67,35.76-8.36-1.56,6.67-35.76,8.36,1.56Z" />
                <path className="cls-6" d="M520.3,182.65l-4.33,37.87-8.37-3.71.99-7.75-10.75-4.77-5.12,5.92-6.83-3.03,25.4-28.53,9.02,4ZM502.42,198.97l7.04,3.12,1.68-13.22-8.73,10.09Z" />
                <path className="cls-6" d="M549.23,198.74l-19.28,30.84-7.21-4.51,19.28-30.84,7.21,4.51Z" />
                <path className="cls-6" d="M563.2,208.09l8.46,7.19c3.45,2.93,5.61,5.59,6.49,7.99s1.11,5.19.69,8.39c-.42,3.2-1.97,6.38-4.65,9.53-3.37,3.96-7.36,6.28-11.96,6.95-4.6.67-8.96-.74-13.07-4.24l-9.53-8.11,23.57-27.7ZM565.24,218.6l-14.8,17.39,2.67,2.27c2.36,2.01,4.87,2.62,7.52,1.83,2.65-.79,4.99-2.38,7.01-4.76,1.43-1.68,2.42-3.51,2.98-5.49s.6-3.68.11-5.08c-.48-1.4-1.41-2.68-2.77-3.84l-2.73-2.33Z" />
              </g>
              <g>
                <polygon className="cls-1" points="398.71 511.11 398.71 421.4 308.99 421.4 308.99 377.82 398.71 377.82 398.71 288.1 442.28 288.1 442.28 377.82 532 377.82 532 421.4 442.28 421.4 442.28 511.11 398.71 511.11" />
                <path className="cls-6" d="M439.28,291.1v89.72h89.72v37.58h-89.72v89.72h-37.58v-89.72h-89.72v-37.58h89.72v-89.72h37.58M445.28,285.1h-49.58v89.72h-89.72v49.58h89.72v89.72h49.58v-89.72h89.72v-49.58h-89.72v-89.72h0Z" />
              </g>
            </svg>
          </div>
          <p className="text-gray-600">
            {translatedContent.main.section1.description[language]}
          </p>
        </section>

        <section className="mb-12">
          <div className="grid grid-cols-1 gap-8 md:grid-cols-3">
            <div className="shadow-md card bg-primary">
              <div className="card-body">
                <h3 className="card-title">{translatedContent.main.section2.card1.title[language]}</h3>
                <p className="text-gray-600">{translatedContent.main.section2.card1.description[language]}</p>
                <div className="justify-end card-actions">
                  <a href="/list" className="btn btn-accent">{translatedContent.main.section2.card1.button[language]}</a>
                </div>
              </div>
            </div>

            <div className="shadow-md card bg-secondary">
              <div className="card-body">
                <h3 className="card-title">{translatedContent.main.section2.card2.title[language]}</h3>
                <p className="text-gray-600">{translatedContent.main.section2.card2.description[language]}</p>
                {/* <div className="shadow-lg alert alert-warning">
                  <div>
                    <svg xmlns="http://www.w3.org/2000/svg" className="flex-shrink-0 w-6 h-6 stroke-current" fill="none" viewBox="0 0 24 24">
                      <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                    </svg>
                    <p className="text-left">{translatedContent.main.section2.card2.alert[language]}</p>
                  </div>
                </div> */}
                <div className="justify-end card-actions">
                  <a href="/apply" className="btn btn-accent">{translatedContent.main.section2.card2.button[language]}</a>
                </div>
              </div>
            </div>

            <div className="bg-white shadow-md card">
              <div className="card-body">
                <h3 className="card-title">Volunteer</h3>
                {/* <p className="text-gray-600">{translatedContent.main.section2.card3.description[language]}</p> */}
                <div className="justify-end card-actions">
                  <a href="/volunteer" className="btn btn-outline btn-primary">{translatedContent.main.section2.card3.button1[language]}</a>
                </div>
              </div>
            </div>

            <div className="bg-white shadow-md card">
              <div className="card-body">
                <h3 className="card-title">{translatedContent.main.section2.card4.title[language]}</h3>
                <p className="text-gray-600">{translatedContent.main.section2.card4.description[language]}</p>
                <div className="justify-end card-actions">
                  {/* <a href="https://t.me/nabaliaidnetwork" target="_blank" rel="noopener noreferrer" className="btn btn-outline btn-primary">{translatedContent.main.section2.card4.button1[language]}</a> */}
                  <a href="https://tiktok.com/@nabaliaid" target="_blank" rel="noopener noreferrer" className="btn btn-outline btn-primary">{translatedContent.main.section2.card4.button2[language]}</a>
                </div>
              </div>
            </div>

          </div>
        </section>
      </main>
    </div>
  );
};