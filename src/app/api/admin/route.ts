import { NextResponse } from "next/server";
const { parsePhoneNumber } = require("libphonenumber-js");

const url = process.env.SHEETS_URL

const formatPhoneNumber = (phoneNumber) => { //helper function used below
  try {
    const parsedNumber = parsePhoneNumber("+" + phoneNumber);
    if (parsedNumber.isValid()) {
      return parsedNumber.formatInternational();
    } else {
      return phoneNumber;
    }
  } catch (error) {
    return phoneNumber;
  }
};

export async function GET(request: Request) {
  console.log(request.url); // do not remove. it keep the route from returning old data
  try {
    const response = await fetch(url);
    const data = await response.json();
    const publishableData = data
      .map((d: any) => {
        return { ...d, formatted_whatsapp: formatPhoneNumber(d.whatsapp) }; // formats the number to make it easier for volunteers to see the country code
      })
      .filter((d: any) => d.hasWhatsAppGroup !== "TRUE"); // only shows families that need to be onboarded still
    return NextResponse.json(publishableData, { cache: "no-store" });
  } catch (error) {
    console.error("Error fetching data from SheetDB:", error);
    return NextResponse.json(
      { error: "An error occurred while fetching data from SheetDB" },
      { status: 500 }
    );
  }
}

export async function POST(request: Request) {
  const data = await request.json();

  try {
    const {id, ...everythingButId} = data // allows me to separate the id from the other stuff without knowing all the names of the properties
    const responseFromDB = await fetch(
      `${url}/id/${data.id}`,
      {
        method: "PATCH",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          data:{
            ...everythingButId
          }
        }),
      }
    );

    const jsonResponseFromDB = await responseFromDB.json();

    return NextResponse.json(jsonResponseFromDB);
  } catch (error) {
    console.error("Error fetching data from SheetDB:", error);
    return NextResponse.json(
      { error: "An error occurred while fetching data from SheetDB" },
      { status: 500 }
    );
  }
}
