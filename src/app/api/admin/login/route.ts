import { NextResponse } from "next/server";

export async function POST(request: Request) {
  const { password } = await request.json();
  console.log({ password, admin: process.env.ADMIN_PASSWORD });
  if (password === process.env.ADMIN_PASSWORD) {
    const response = new NextResponse(null, { status: 200 });
    response.cookies.set("adminPassword", password, { httpOnly: true });
    return response;
  }

  return new NextResponse(JSON.stringify({ error: "Invalid password" }), {
    status: 401,
  });
}