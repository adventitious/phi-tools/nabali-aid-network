import { NextResponse } from "next/server";
const translatte = require("translatte");

const url = process.env.SHEETS_URL;
console.log({url})
export async function GET() {
  try {
    const response = await fetch(url); // Next.js support fetch
    const data = await response.json();
    const publishableData = data
      .filter((d: any) => d.publish === "TRUE")
      .map((d: any) => {
        const { whatsapp, ...rest } = d;
        return {
          ...rest,
        };
      });
    return NextResponse.json(publishableData);
  } catch (error) {
    console.error("Error fetching data from SheetDB:", error);
    return NextResponse.json(
      { error: "An error occurred while fetching data from SheetDB" },
      { status: 500 }
    );
  }
}
export async function POST(request: Request) {
  const formData = await request.json();
  console.log({ formData });

  try {
    // Search for an existing record with the same whatsapp number
    const searchResponse = await fetch(
      `${url}/search?whatsapp=${formData.whatsapp}`
    );
    const searchData = await searchResponse.json();

    if (searchData.length > 0) {
      // Duplicate record found
      return NextResponse.json(
        { message: "You are already on the list." },
        { status: 400 }
      );
    }
    console.log({before: {
      ...formData,
      id: "INCREMENT",
      names: formData.language === "en" ? formData.familyMembers.join() : "",
      names_ar: formData.language !== "en" ? formData.familyMembers.join() : "",
      medical_need: formData.language === "en" ? formData.medicalNeed : "",
      medical_need_ar: formData.language !== "en" ? formData.medicalNeed_ar : "",
      future_plans: formData.language === "en" ? formData.futurePlans : "",
      future_plans_ar: formData.language !== "en" ? formData.futurePlans_ar : "",
      age_of_youngest: formData.ageOfYoungest,
      age_of_oldest: formData.ageOfOldest,
      publish: "FALSE",
      hasWhatsAppGroup: "FALSE",
      OOBCheckDone: "", // TODO: add OOB check, turning out to be more diff
    }});
    // No duplicate found, proceed with adding the new record
    let translatedData = await translateObject({
      ...formData,
      id: "INCREMENT",
      names: formData.language === "en" ? formData.familyMembers.join() : "",
      names_ar: formData.language !== "en" ? formData.familyMembers.join() : "",
      medical_need: formData.language === "en" ? formData.medicalNeed : "",
      medical_need_ar: formData.language !== "en" ? formData.medicalNeed_ar : "",
      future_plans: formData.language === "en" ? formData.futurePlans : "",
      future_plans_ar: formData.language !== "en" ? formData.futurePlans_ar : "",
      age_of_youngest: formData.ageOfYoungest,
      age_of_oldest: formData.ageOfOldest,
      publish: "FALSE",
      hasWhatsAppGroup: "FALSE",
      OOBCheckDone: "", // TODO: add OOB check, turning out to be more diff
    });

    console.log({ translatedData});

    if (translatedData) {
      let data = {
        data: [
          // TODO: translate the information before submitting
          // i already did this work lol nvm
          translatedData,
        ],
      };

      const responseFromDB = await fetch(
        url,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        }
      );

      const jsonResponseFromDB = await responseFromDB.json();

      return NextResponse.json(jsonResponseFromDB);
    } else {
      throw "Error fetching translations"
    }
  } catch (error) {
    console.error("Error fetching data from SheetDB:", error);
    return NextResponse.json(
      { error: "An error occurred while fetching data from SheetDB" },
      { status: 500 }
    );
  }
}

async function translateObject(obj: any) {
  let promises = [];

  for (let key in obj) {
    if (key.endsWith("_ar")) {
      if (obj.language !== "en") {
        if (obj[key]) {
          let newKey = key.slice(0, -3); // Remove the "_ar" suffix

          console.log(key, key.slice(0, -3));
          let promise = translatte(obj[key], { from: "ar", to: "en" })
            .then((res) => {
              obj[newKey] = res.text; // Save the translated text in the new key
            })
            .catch((err) => {
              console.error(err);
            });
          promises.push(promise);
        }
      } else {
        if (obj[key.slice(0, -3)]) {
          let promise = translatte(obj[key.slice(0, -3)], {
            from: "en",
            to: "ar",
          })
            .then((res) => {
              obj[key] = res.text; // Save the translated text in the new key
            })
            .catch((err) => {
              console.error(err);
            });
          promises.push(promise);
        }
      }
    }
  }

  await Promise.all(promises);
  return obj;
}
