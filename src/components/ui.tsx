import React from 'react'

const Header = ({ language, switchLanguage }) => (
    <header className="bg-white shadow w=full">
        <div className="container px-4 py-6 mx-auto">
            <a href="/">
                <h1 className="text-3xl font-bold text-gray-800">{language === "en" ? "Nabali Aid Network" : "شبكة المساعدات النبالية"}</h1>
            </a>
        </div>
    </header>
);

const LanguageSwitcher = ({ language, switchLanguage }) => (
    <div className="px-4 mt-4 mb-4 text-right">
        <button
            className={`btn btn-sm btn-outline ${language === 'en' ? 'btn-active' : ''} mr-4`}
            onClick={() => switchLanguage('en')}
        >
            English
        </button>
        <button
            className={`btn btn-sm btn-outline ${language === 'ar' ? 'btn-active' : ''}`}
            onClick={() => switchLanguage('ar')}
        >
            العربية
        </button>
    </div>
);

function BilingualLabel({ language, englishContent, arabicContent, emoji, htmlFor, required, error }: { language: string, englishContent: string, arabicContent: string, emoji: any, htmlFor: string, required: boolean, error: boolean }) {
    return (
        <label htmlFor={htmlFor} className={`flex items-center mb-2 font-bold ${error ? 'text-error' : ''}`}>
            {emoji} {language === 'en' ? englishContent : arabicContent}{required && "*"}
        </label>
    )
}

function Video({ src }) {
    return (
        <video style={{ height: '300px' }} height="300" width="200" controls preload="none">
            <source src={src} type="video/mp4" />
            Your browser does not support the video tag.
        </video>
    )
}

export {
    BilingualLabel,
    Header,
    LanguageSwitcher,
    Video
}