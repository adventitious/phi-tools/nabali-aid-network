const theme = {
    "primary": "#B1CC33",
    "secondary": "#EA5A47",
    "accent": "#facc15",
    "neutral": "#0f1201",
    "base-100": "#ecfccb",
    "info": "#06b6d4",
    "success": "#22c55e",
    "warning": "#dc2626",
    "error": "#dc2626",
    "--fallback-p": "#B1CC33", // the fallbacks are effectively safari colors
    "--fallback-pc": "#0f1201", // primary content
    "--fallback-s": "#EA5A47",
    "--fallback-sc": "#0f1201", // secondary content
    "--fallback-a": "#facc15",
    "--fallback-ac": "#00100d", //accent content
    "--fallback-n": "#0f1201",
    "--fallback-nc": "#f6fee7", // neutral content
    "--fallback-b1": "#ecfccb", // base-100
    "--fallback-b2": "#dbf99d", // generated using https://uicolors.app/create and base 100 color
    "--fallback-b3": "#c4f264", // generated using https://uicolors.app/create and base 100 color 
    "--fallback-bc": "#0f1201", //base content
    "--fallback-in": "#06b6d4",
    "--fallback-inc": "#000000", //info content
    "--fallback-su": "#22c55e",
    "--fallback-suc": "#000000", //success content
    "--fallback-wa": "#ffc22d",
    "--fallback-wac": "#000000", //warning content
    "--fallback-er": "#ff6f70",
    "--fallback-erc": "#000000", //error content
}
export default theme;